//If you need to use NULL do for example:
udr0 = '\0';

//If you want to use nop on assembly:
asm("nop");